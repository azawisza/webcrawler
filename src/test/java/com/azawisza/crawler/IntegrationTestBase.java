package com.azawisza.crawler;

import com.google.common.collect.ImmutableList;
import com.azawisza.crawler.crawler.dao.UrlEntity;
import com.azawisza.crawler.crawler.dao.UrlsRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

/**
 * Created by azawisza on 23.10.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TestWebConf.class})

public abstract class IntegrationTestBase {

    @Autowired
    protected WebApplicationContext context;
    protected MockMvc mockMvc;

    @Autowired
    protected UrlsRepository repository;



    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }



    protected List<UrlEntity> pollIfUrlInRepository(String url, int times) {
        int millis = 300;
        int counter = 0 ;
        List<UrlEntity> url1 = ImmutableList.of();
        while (url1.isEmpty() && counter < times) {
            url1 = repository.findUrl(url);
            System.out.println("Check " + url1 + " ...");
            try {
                Thread.currentThread().sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        return url1;
    }

}
