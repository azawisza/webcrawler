package com.azawisza.crawler.webui;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.azawisza.crawler.IntegrationTestBase;
import com.azawisza.crawler.api.model.InvestigateUrlsRQ;
import com.azawisza.crawler.api.model.InvestigateUrlsRS;
import com.azawisza.crawler.api.model.UrlToCheck;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

import static com.google.common.collect.ImmutableList.of;
import static com.azawisza.crawler.api.model.UrlsSubmitResultStatus.INVALID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ValidationIntegrationTest extends IntegrationTestBase {

    private Gson gson = new Gson();

    @Test
    public void shouldShowInvalidBecauseOfEmptyCredentials() throws Exception {

        String json = gson.toJson(new InvestigateUrlsRQ()
                .withUrls());
        String expected = gson.toJson(new InvestigateUrlsRS()
                .withStatus(INVALID).withErrors(
                        ImmutableList.of("The url list may not be empty")));


        this.mockMvc.perform(post("/crawlurls/")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(content().json(expected))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldValidateIncorrectUrl() throws Exception {
        //given
        String json = gson.toJson(new InvestigateUrlsRQ()
                .withUrls(new UrlToCheck("http://c-and-a.com"),
                        new UrlToCheck("toshiba.es"),
                        new UrlToCheck("d3k9d2d_notcorrect")
                ));
        String expected = gson.toJson(new InvestigateUrlsRS()
                .withStatus(INVALID).withErrors(of("must be a valid URL", "must be a valid URL")));
        //when
        ResultActions perform = this.mockMvc.perform(post("/crawlurls/")
                .contentType(MediaType.APPLICATION_JSON).content(json));
        //then
        perform.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(contentOf(expected))
                .andExpect(status().isOk());
    }



    protected ResultMatcher contentOf(String expected) {
        return result -> {
            MockHttpServletResponse response = result.getResponse();
            String contentAsString = response.getContentAsString();
            Assertions.assertThat(contentAsString).isEqualTo(expected);
        };
    }




}