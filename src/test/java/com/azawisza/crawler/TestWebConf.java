package com.azawisza.crawler;

import com.azawisza.crawler.crawler.DocumentRetriever;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Optional;

/**
 * Created by azawisza on 23.10.2016.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.azawisza.crawler", "com.azawisza.application", "com.azawisza.commons"})
@PropertySource("classpath:application-test.yml")
@EnableTransactionManagement
public class TestWebConf extends WebMvcConfigurerAdapter {

    @Bean
    public PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(new ClassPathResource("application-test.yml"));
        propertySourcesPlaceholderConfigurer.setProperties(yaml.getObject());
        return propertySourcesPlaceholderConfigurer;
    }

    @Bean
    @Primary
    public DocumentRetriever mockDocumentRetriever(){
        DocumentRetriever mock = Mockito.mock(DocumentRetriever.class);
        Document mockOfWebsite = Jsoup.parse("<html><title>news</title><body></body></html>");
        Mockito.when(mock.retrieve(Mockito.anyString())).thenReturn(Optional.of(mockOfWebsite));
        return mock;
    }
}
