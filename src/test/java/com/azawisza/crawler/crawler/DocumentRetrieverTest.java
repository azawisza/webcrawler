package com.azawisza.crawler.crawler;

import org.fest.assertions.Assertions;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.util.Optional;

/**
 * Created by azawisza on 23.10.2016.
 */
public class DocumentRetrieverTest {

    @Test
    public void shouldNotReachAndReturnEmpty() {
        //given
        DocumentRetriever retriever = new DocumentRetriever();
        //when
        Optional<Document> retrieve = retriever.retrieve("http://willnotreachthis");
        //then
        Assertions.assertThat(retrieve.isPresent()).isFalse();
    }

}