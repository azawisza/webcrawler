package com.azawisza.crawler.service;

import com.azawisza.crawler.IntegrationTestBase;
import com.azawisza.crawler.crawler.dao.UrlAggregate;
import com.azawisza.crawler.crawler.dao.UrlEntity;
import com.azawisza.crawler.crawler.dao.UrlsRepository;
import com.azawisza.crawler.api.model.UrlsSubmitResultStatus;
import com.azawisza.crawler.api.model.UrlToCheck;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.*;

import java.util.List;

import static com.google.common.collect.ImmutableList.of;
import static com.azawisza.crawler.api.model.UrlsSubmitResultStatus.ADDED;
import static org.fest.assertions.Assertions.assertThat;
/**
 * Created by azawisza on 23.10.2016.
 */
public class UserServiceImplTest extends IntegrationTestBase{

    @Autowired
    private UrlService service;

    @Autowired
    private UrlAggregate urlAggregate;

    @Autowired
    private UrlsRepository repository;

    @PersistenceUnit
    private EntityManagerFactory factory;

    private List<UrlToCheck> urlsToCheck = of(new UrlToCheck("this.is.url.pl"));


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        Query nativeQuery = entityManager.createNativeQuery("TRUNCATE TABLE URLS");
        nativeQuery.executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void shouldSubmitUrls() {
        //given
        //when
        UrlsSubmitResultStatus userStatus = service.investigate(urlsToCheck);
        //then
        assertThat(userStatus).isEqualTo(ADDED);
    }

    @Test
    public void shouldAddUserIfAbsent() {
        //given
        //when
        UrlsSubmitResultStatus userStatus = service.investigate(urlsToCheck);
        List<UrlEntity> url = repository.findUrl("this.is.url.pl");
        //then
        assertThat(userStatus).isEqualTo(ADDED);
    }


}