package com.azawisza.crawler.service;

import com.google.gson.Gson;
import com.azawisza.crawler.IntegrationTestBase;
import com.azawisza.crawler.crawler.DocumentRetriever;
import com.azawisza.crawler.crawler.dao.UrlAggregate;
import com.azawisza.crawler.crawler.dao.UrlEntity;
import com.azawisza.crawler.crawler.dao.UrlsRepository;
import com.azawisza.crawler.api.model.InvestigateUrlsRQ;
import com.azawisza.crawler.api.model.InvestigateUrlsRS;
import com.azawisza.crawler.api.model.UrlToCheck;
import org.fest.assertions.Assertions;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.ImmutableList.of;
import static com.azawisza.crawler.api.model.UrlsSubmitResultStatus.ADDED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Created by azawisza on 23.10.2016.
 */
public class IntegrationTest extends IntegrationTestBase {

    @Autowired
    public UrlsRepository repository;

    @Autowired
    public UrlAggregate urlAggregate;

    @Autowired
    public DocumentRetriever retriever;

    private Gson gson = new Gson();


    @PersistenceUnit
    private EntityManagerFactory factory;


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        EntityManager entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        Query nativeQuery = entityManager.createNativeQuery("TRUNCATE TABLE URLS");
        nativeQuery.executeUpdate();
        entityManager.getTransaction().commit();
    }

    @Test
    public void shouldAddNewUrl() throws Exception {
        //given
        String url = "http://onet.pl";
        String json = gson.toJson(new InvestigateUrlsRQ()
                .withUrls(new UrlToCheck(url)));
        String expected = gson.toJson(new InvestigateUrlsRS()
                .withStatus(ADDED));
        Document mockOfWebsite = Jsoup.parse("<html><title>noticias</title><body></body></html>");
        Mockito.when(retriever.retrieve(Mockito.anyString())).thenReturn(Optional.of(mockOfWebsite));
        //when
        this.mockMvc.perform(post("/crawlurls/")
                .contentType(MediaType.APPLICATION_JSON).content(json));
        List<UrlEntity> actual = pollIfUrlInRepository(url, 10);

        //then
        Assertions.assertThat(actual.get(0).getUrl()).isEqualTo(url);
        Assertions.assertThat(actual.get(0).isCompatible()).isEqualTo(true);
    }

    @Test
    public void shouldAddNewUrlAsNotMarfelizzable() throws Exception {
        //given
        String url = "http://onet.pl";
        String json = gson.toJson(new InvestigateUrlsRQ()
                .withUrls(new UrlToCheck(url)));
        Document mockOfWebsite = Jsoup.parse("<html><title></title><body></body></html>");
        Mockito.when(retriever.retrieve(Mockito.anyString())).thenReturn(Optional.of(mockOfWebsite));

        //when
        this.mockMvc.perform(post("/crawlurls/")
                .contentType(MediaType.APPLICATION_JSON).content(json));
        List<UrlEntity> actual = pollIfUrlInRepository(url, 20);

        //then
        Assertions.assertThat(actual).isNotEmpty();
        Assertions.assertThat(actual.get(0).getUrl()).isEqualTo(url);
        Assertions.assertThat(actual.get(0).isCompatible()).isEqualTo(false);
    }


    @Test
    public void shouldNotSaveIfException() throws Exception {
        //given
        String url = "http://onet.pl";
        String json = gson.toJson(new InvestigateUrlsRQ()
                .withUrls(new UrlToCheck(url)));
        String expected = gson.toJson(new InvestigateUrlsRS()
                .withStatus(ADDED));
        Mockito.when(retriever.retrieve(Mockito.anyString())).thenReturn(null);//will fail

        //when
        this.mockMvc.perform(post("/crawlurls/")
                .contentType(MediaType.APPLICATION_JSON).content(json));
        List<UrlEntity> actual = pollIfUrlInRepository(url, 4);

        //then
        Assertions.assertThat(actual).isEmpty();
    }

    @After
    public void tearDown(){
        Mockito.reset(retriever);
    }

}