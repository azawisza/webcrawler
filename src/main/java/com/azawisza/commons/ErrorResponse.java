package com.azawisza.commons;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
/**
 * Created by azawisza on 23.10.2016.
 */
public class ErrorResponse {

    public String message;
    public String details;
    public String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ErrorResponse withMessage(final String message) {
        this.message = message;
        return this;
    }

    public ErrorResponse withDetails(final String details) {
        this.details = details;
        return this;
    }

    public ErrorResponse withCode(final String code) {
        this.code = code;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
