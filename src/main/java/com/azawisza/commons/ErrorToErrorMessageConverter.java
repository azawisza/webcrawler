package com.azawisza.commons;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by azawisza on 23.10.2016.
 */
@Component
public class ErrorToErrorMessageConverter {

    public List<String> convert(Errors errors) {
        List<String> collect = errors.getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return collect;
    }

}