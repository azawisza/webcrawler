package com.azawisza.crawler.service;

import com.azawisza.crawler.api.model.UrlsSubmitResultStatus;
import com.azawisza.crawler.crawler.WebCrawler;
import com.azawisza.crawler.crawler.dao.UrlsRepository;
import com.azawisza.crawler.api.model.UrlToCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * Created by azawisza on 23.10.2016.
 */
@Component
@Primary
public class UrlServiceImpl implements UrlService {

    private final UrlsRepository urlsRepository;
    private final WebCrawler webCrawler;

    @Autowired
    public UrlServiceImpl(UrlsRepository urlsRepository, WebCrawler webCrawler) {
        this.urlsRepository = urlsRepository;
        this.webCrawler = webCrawler;
    }

    @Override
    public UrlsSubmitResultStatus investigate(List<UrlToCheck> urlToChecks) {
        for (UrlToCheck url : urlToChecks) {
            webCrawler.submit(url.getUrl());
        }
        return UrlsSubmitResultStatus.ADDED;
    }
}
