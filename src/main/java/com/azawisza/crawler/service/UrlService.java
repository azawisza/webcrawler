package com.azawisza.crawler.service;

import com.azawisza.crawler.api.model.UrlsSubmitResultStatus;
import com.azawisza.crawler.api.model.UrlToCheck;

import java.util.List;

/**
 * Created by cx2 on 2015-06-27.
 */
public interface UrlService {

    public UrlsSubmitResultStatus investigate(List<UrlToCheck> urlToChecks);

}
