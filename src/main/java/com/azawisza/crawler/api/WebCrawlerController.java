package com.azawisza.crawler.api;

import com.azawisza.crawler.api.model.InvestigateUrlsRQ;
import com.azawisza.crawler.api.model.InvestigateUrlsRS;
import com.azawisza.crawler.api.model.UrlsSubmitResultStatus;
import com.azawisza.commons.ErrorToErrorMessageConverter;
import com.azawisza.crawler.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Logger;


@RestController
public class WebCrawlerController {


    private final UrlService websiteCrwalingService;
    private final ErrorToErrorMessageConverter errorConverter;
    final static Logger logger = Logger.getLogger(WebCrawlerController.class.getName());

    @Autowired
    public WebCrawlerController(UrlService crawlingService, ErrorToErrorMessageConverter errorConverter) {
        this.websiteCrwalingService = crawlingService;
        this.errorConverter = errorConverter;
    }

    public
    @RequestMapping(value = "/crawlurls/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    InvestigateUrlsRS crawlUrls(@RequestBody @Valid InvestigateUrlsRQ request, Errors errors) {
        logger.info(request.toString());
        InvestigateUrlsRS response = new InvestigateUrlsRS();
        if (errors.hasErrors()) {
            InvestigateUrlsRS investigateUrlsRS = response.withErrors(errorConverter.convert(errors))
                    .withStatus(UrlsSubmitResultStatus.INVALID);
            return investigateUrlsRS;
        } else {
            UrlsSubmitResultStatus status = websiteCrwalingService.investigate(request.getUrls());
            return response.withStatus(status);
        }
    }

}
