package com.azawisza.crawler.api.model;

import com.azawisza.commons.ToStringSettings;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.URL;

/**
 * Created by azawisza on 23.10.2016.
 */
public class UrlToCheck {

    @URL
    private String url;

    public UrlToCheck() {
    }

    public UrlToCheck(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UrlToCheck withUrl(final String url) {
        this.url = url;
        return this;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return ToStringSettings.getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
