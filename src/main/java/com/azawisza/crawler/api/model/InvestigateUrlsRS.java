package com.azawisza.crawler.api.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

import static com.azawisza.commons.ToStringSettings.getToStringBuilder;
/**
 * Created by azawisza on 23.10.2016.
 */
public class InvestigateUrlsRS {

    private List<String> errors = new ArrayList<String>();
    private UrlsSubmitResultStatus status;

    public InvestigateUrlsRS withErrors(final List<String> errors) {
        this.errors = errors;
        return this;
    }

    public InvestigateUrlsRS withStatus(final UrlsSubmitResultStatus status) {
        this.status = status;
        return this;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public UrlsSubmitResultStatus getStatus() {
        return status;
    }

    public void setStatus(UrlsSubmitResultStatus status) {
        this.status = status;
    }


    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
