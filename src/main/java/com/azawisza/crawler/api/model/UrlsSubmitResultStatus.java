package com.azawisza.crawler.api.model;
/**
 * Created by azawisza on 23.10.2016.
 */
public enum UrlsSubmitResultStatus {
    ADDED,
    INVALID
}
