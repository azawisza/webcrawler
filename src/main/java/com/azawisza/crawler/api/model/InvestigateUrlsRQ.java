package com.azawisza.crawler.api.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.azawisza.commons.ToStringSettings.getToStringBuilder;

/**
 * Created by cx2 on 2015-06-27.
 */
public class InvestigateUrlsRQ {

    @Valid
    @NotEmpty(message = "The url list may not be empty")
    private List<UrlToCheck> urls;

    public InvestigateUrlsRQ() {
    }

    public InvestigateUrlsRQ withUrls(final List<UrlToCheck> urls) {
        this.urls = urls;
        return this;
    }

    public InvestigateUrlsRQ withUrls(final UrlToCheck... value) {
        List<UrlToCheck> valuesAsList = Arrays.asList(value);
        if (urls == null) {
            urls = new ArrayList<>();
            urls.addAll(valuesAsList);
        } else {
            urls.addAll(valuesAsList);
        }
        return this;
    }

    public List<UrlToCheck> getUrls() {
        return urls;
    }

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    public String toString() {
        return getToStringBuilder(this).reflectionToString(this);
    }

    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
