package com.azawisza.crawler.crawler;

import com.azawisza.crawler.crawler.dao.UrlAggregate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;




/**
 * Created by azawisza on 23.10.2016.
 */
@Component
public class WebCrawler {

    private final CrawlerConfiguration conf;
    private final UrlAggregate aggregate;
    private final WebsiteVerificator websiteVerificator;
    private final ExecutorService service;

    static final Logger logger = Logger.getLogger(WebCrawler.class.getName());

    @Autowired
    public WebCrawler(CrawlerConfiguration conf, UrlAggregate aggregate, WebsiteVerificator websiteVerificator) {
        this.conf = conf;
        this.aggregate = aggregate;
        this.websiteVerificator = websiteVerificator;
        service = createThreadPool();
    }

    public void submit(String url) {
        CrawlerTask task = new CrawlerTask(url, aggregate, websiteVerificator);
        service.submit(task);
    }

    private ExecutorService createThreadPool() {
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(conf.getMaxWaitingRq());
        ExecutorService service = new ThreadPoolExecutor(
                conf.getCorePoolSize(),
                conf.getMaximumPoolSize(),
                conf.getKeepAliveTime(),
                conf.getKeepAliveUnit(),
                workQueue);
        return service;
    }

    @PreDestroy
    public void shutDown() {
        logger.info("Shutting down executor service ...");
        if (!service.isShutdown()) {
            service.shutdown();
            try {
                service.awaitTermination(conf.getSutDownTimeout(), TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.warning(" Exception on shutting down executor service !");
            }
            logger.info("Shutting down executor service ... done");
        }
    }

}
