package com.azawisza.crawler.crawler;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by azawisza on 23.10.2016.
 */
@Component
public class WebsiteVerificator {

    private static final String TITLE_TAG = "title";

    private DocumentRetriever documentRetriever;
    private Predicate<String> isNoticias = (v) -> v.contains("noticias");
    private Predicate<String> isNews = (v) -> v.contains("news");
    private Map<String, Predicate<String>> conditions = new HashMap<>();

    {
        conditions.put(TITLE_TAG, isNews.or(isNoticias));
    }

    public WebsiteVerificator(DocumentRetriever documentRetriever) {
        this.documentRetriever = documentRetriever;
    }

    public boolean isEligibleForParsing(String url) {
        final Optional<Document> document = documentRetriever.retrieve(url);
        return document.isPresent() && matchesAnyConditionOfParsing(document.get());
    }

    private boolean matchesAnyConditionOfParsing(Document document) {
        boolean atLeastOneMatching = conditions.entrySet().stream().filter((entry) -> {
            String tagName = entry.getKey();
            Optional<String> tagText = getTagText(document, tagName);
            boolean willNotFilterOutElement = false;
            if (tagText.isPresent()) {
                willNotFilterOutElement = entry.getValue().test(tagText.get());
            }
            return willNotFilterOutElement;
        }).findFirst().isPresent();
        return atLeastOneMatching;
    }

    private Optional<String> getTagText(Document document, String tagName) {
        Elements select = document.select(tagName);
        if (select != null && select.size() > 0) {
            Element firstTitle = select.get(0);
            if (firstTitle.hasText()) {
                return Optional.of(firstTitle.text());
            }
        }
        return Optional.empty();
    }


}
