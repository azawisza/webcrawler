package com.azawisza.crawler.crawler;

import com.azawisza.crawler.crawler.dao.UrlAggregate;


import java.util.logging.Logger;


/**
 * Created by azawisza on 23.10.2016.
 */
public class CrawlerTask implements Runnable {

    private final String url;
    private final WebsiteVerificator verificator;
    private final UrlAggregate aggregate;

    private static final Logger logger = Logger.getLogger(CrawlerTask.class.getName());

    public CrawlerTask(String url, UrlAggregate aggregate, WebsiteVerificator verificator) {
        this.url = url;
        this.verificator = verificator;
        this.aggregate = aggregate;
    }

    @Override
    public void run() {
        StringBuilder report = new StringBuilder();
        report.append("Verification of ").append(url);
        try {
            boolean result = verificator.isEligibleForParsing(url);
            report.append("Is parsable: ").append(result).append(" ");
            report.append("Saving to database ...");
            aggregate.updateUrl(url, result);
            report.append("Saving to database ...done");
        } catch (Exception e) {
            e.printStackTrace();
            report.append(" EXCEPTION !");
            //send to exception handling service
        } finally {
            logger.info(report.toString());
        }
    }

}
