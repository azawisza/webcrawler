package com.azawisza.crawler.crawler.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by azawisza on 23.10.2016.
 */
@Repository
public class UrlsRepository {

    @PersistenceContext
    public EntityManager entityManager;

    @Transactional
    public List<UrlEntity> findUrl(String url) {
        try {
            List<UrlEntity> resultList = entityManager.createQuery("select c from UrlEntity c where url=:p_url", UrlEntity.class)
                    .setParameter("p_url", url)
                    .getResultList();
            return resultList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  new ArrayList<>();
    }

    @Transactional
    public void addUrl(UrlEntity url) {
        entityManager.merge(url);
    }

}
