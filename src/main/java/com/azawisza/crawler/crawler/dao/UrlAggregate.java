package com.azawisza.crawler.crawler.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.logging.Logger;


/**
 * Created by azawisza on 23.10.2016.
 */
@Component
public class UrlAggregate {

    private final UrlsRepository repository;
    private static final Logger logger = Logger.getLogger(UrlAggregate.class.getName());

    @Autowired
    public UrlAggregate(UrlsRepository urlsRepository) {
        this.repository = urlsRepository;
    }

    @Transactional
    public void updateUrl(String url, boolean result) {
        List<UrlEntity> urlFound = repository.findUrl(url);
        UrlEntity urlEntity;
        if (urlFound != null && !urlFound.isEmpty()) {
            validateSize(urlFound);
            urlEntity = urlFound.get(0);
            urlEntity.setCompatible(result);
        } else {
            urlEntity = new UrlEntity()
                    .withUrl(url)
                    .withIsCompatible(result);
            repository.addUrl(urlEntity);
        }

    }

    private void validateSize(List<UrlEntity> urlFound) {
        if (urlFound.size() > 1) {
            logger.warning("Warning - Multiple urls found");
        }
    }

}
