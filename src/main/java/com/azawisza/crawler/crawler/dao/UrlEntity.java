package com.azawisza.crawler.crawler.dao;

import javax.persistence.*;
/**
 * Created by azawisza on 23.10.2016.
 */
@Entity
@Table(name = "urls")
public class UrlEntity {

    @Id
    @GeneratedValue
    @Column
    public long urlId;

    @Column(unique = true, nullable = false)
    private String url;

    @Column(nullable = false)
    private boolean isCompatible;


    public long getUrlId() {
        return urlId;
    }

    public void setUrlId(long urlId) {
        this.urlId = urlId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isCompatible() {
        return isCompatible;
    }

    public void setCompatible(boolean compatible) {
        isCompatible = compatible;
    }

    public UrlEntity withUrl(final String url) {
        this.url = url;
        return this;
    }

    public UrlEntity withIsCompatible(final boolean isCompatible) {
        this.isCompatible = isCompatible;
        return this;
    }


}
