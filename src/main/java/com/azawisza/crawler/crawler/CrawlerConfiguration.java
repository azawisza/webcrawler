package com.azawisza.crawler.crawler;

import java.util.concurrent.TimeUnit;

/**
 * Created by azawisza on 23.10.2016.
 */
public class CrawlerConfiguration {

    private int corePoolSize;
    private int maximumPoolSize;
    private long keepAliveTime;
    private TimeUnit keepAliveUnit;
    private int maxWaitingRq;
    private int sutDownTimeout;

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    public TimeUnit getKeepAliveUnit() {
        return keepAliveUnit;
    }

    public int getMaxWaitingRq() {
        return maxWaitingRq;
    }

    public int getSutDownTimeout() {
        return sutDownTimeout;
    }

    public CrawlerConfiguration withCorePoolSize(final int corePoolSize) {
        this.corePoolSize = corePoolSize;
        return this;
    }

    public CrawlerConfiguration withMaximumPoolSize(final int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
        return this;
    }

    public CrawlerConfiguration withKeepAliveTime(final long keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
        return this;
    }

    public CrawlerConfiguration withKeepAliveUnit(final TimeUnit keepAliveUnit) {
        this.keepAliveUnit = keepAliveUnit;
        return this;
    }

    public CrawlerConfiguration withMaxWaitingRq(final int maxWaitingRq) {
        this.maxWaitingRq = maxWaitingRq;
        return this;
    }

    public CrawlerConfiguration withSutDownTimeout(final int sutDownTimeout) {
        this.sutDownTimeout = sutDownTimeout;
        return this;
    }


}
