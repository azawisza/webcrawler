package com.azawisza.crawler.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;


/**
 * Created by azawisza on 23.10.2016.
 */
@Component
public class DocumentRetriever {

    public Optional<Document> retrieve (String url){
        try {
            Document document = Jsoup.connect(url).get();
            return Optional.of(document);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

}
