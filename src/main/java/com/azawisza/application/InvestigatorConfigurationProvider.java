package com.azawisza.application;

import com.azawisza.crawler.crawler.CrawlerConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by azawisza on 23.10.2016.
 */
@Configuration
public class InvestigatorConfigurationProvider {

    @Value("${executor.corePoolSize}")
    private int corePoolSize;

    @Value("${executor.maximumPoolSize}")
    private int maximumPoolSize;

    @Value("${executor.keepAliveTimeSec}")
    private long keepAliveTime;

    private TimeUnit keepAliveUnit = TimeUnit.SECONDS;

    @Value("${executor.maxWaitingRq}")
    private int maxWaitingRq;

    @Value("${executor.shutDownTimeout}")
    private int shutDownTimeout;

    @Bean
    public CrawlerConfiguration investigatorConfiguration() {
        return new CrawlerConfiguration()
                .withKeepAliveTime(keepAliveTime)
                .withKeepAliveUnit(keepAliveUnit)
                .withCorePoolSize(corePoolSize)
                .withMaximumPoolSize(maximumPoolSize)
                .withMaxWaitingRq(maxWaitingRq)
                .withSutDownTimeout(shutDownTimeout);
    }

}
