package com.azawisza.application;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

/**
 * Created by azawisza on 23.10.2016.
 */
@Configuration
public class JpaConfiguration {

    public static final String ENTITIES_DIRECTORY = "com.azawisza";
    @Autowired
    public BasicDataSource dataSource;
    @Value("${spring.jpa.database-platform}")
    public String dialect;
    @Value("${spring.jpa.hibernate.ddl-auto}")
    public String hbm2ddl;
    @Value("${spring.jpa.properties.hibernate.show_sql}")
    public String show_sql;
    @Value("${spring.jpa.properties.javax.persistence.sharedCache.mode}")
    public String sharedCacheMode;
    @Value("${spring.jpa.hibernate.cache.region.factory_class}")
    public String factoryClass;

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[]{ENTITIES_DIRECTORY});
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        return em;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", hbm2ddl);
        properties.setProperty("hibernate.dialect", dialect);
        properties.setProperty("hibernate.show_sql", show_sql);
        properties.setProperty("javax.persistence.sharedCache.mode", sharedCacheMode);
        //properties.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.EhCacheProvider");
        properties.setProperty("hibernate.cache.region.factory_class", factoryClass);
        //properties.setProperty("hibernate.cache.provider_configuration_file_resource_path", "ehcache.xml");
        properties.setProperty("hibernate.cache.use_query_cache", "true");
        return properties;
    }

}
