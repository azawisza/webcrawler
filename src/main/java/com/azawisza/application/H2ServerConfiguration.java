package com.azawisza.application;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.sql.SQLException;
/**
 * Created by azawisza on 23.10.2016.
 */
@Configuration
public class H2ServerConfiguration {

    @Value("${spring.datasource.db-port}")
    public String dbPort;

    @Value("${spring.datasource.db-web-port}")
    public String dbWebPort;

    @Bean(name = "h2WebServer", destroyMethod = "stop", initMethod = "start")
    public Server createH2WebServer() {
        Server webServer = null;
        try {
            webServer = Server.createWebServer("-web", "-webAllowOthers", "-webPort", dbWebPort);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return webServer;
    }

    @Bean(name = "h2Server", destroyMethod = "stop", initMethod = "start")
    @DependsOn("h2WebServer")
    public Server createH2Server() {
        Server webServer = null;
        try {
            webServer = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", dbPort);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return webServer;
    }

}
